(function($){
	$(document).ready(function(){
		$('.wpb-trigger').on('click', function(){
			var getClass = $(this).data('modal');
			$('#'+getClass).addClass('wpb-show');
		});
		$('.wpb-close').on('click', function(){
			$(this).closest('.wpb-modal').removeClass('wpb-show');
		});
	});
}(jQuery));