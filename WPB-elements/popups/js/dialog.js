(function($){
	$('.wpb_trigger_dialog').on('click',function(){
		var target = $(this).data('dialog');
		$('#'+target).addClass('wpb_dialog--in')
	});
	$('.wpb_action,.wpb_dialog__overlay').on('click',function(){
		var el = $(this);
		el.closest('.wpb_dialog').removeClass('wpb_dialog--in').addClass('wpb_dialog--out');
		setTimeout(function(){
			el.closest('.wpb_dialog').removeClass('wpb_dialog--out');
		},200);
	});
}(jQuery))