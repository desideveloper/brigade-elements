(function($){
	$(function(){
		function getScrollbarWidth() {
		    var outer = document.createElement("div");
		    outer.style.visibility = "hidden";
		    outer.style.width = "100px";
		    document.body.appendChild(outer);

		    var widthNoScroll = outer.offsetWidth;
		    // force scrollbars
		    outer.style.overflow = "scroll";

		    // add innerdiv
		    var inner = document.createElement("div");
		    inner.style.width = "100%";
		    outer.appendChild(inner);

		    var widthWithScroll = inner.offsetWidth;

		    // remove divs
		    outer.parentNode.removeChild(outer);

		    return widthNoScroll - widthWithScroll;
		  }

		  // document.body.innerHTML = "Scrollbar width is: "+getScrollbarWidth()+"px";
		  var scrollbar = getScrollbarWidth();
		  if($('#owl-demo').length && scrollbar == 0){
		    $('#owl-demo').slick({
		      centerMode: true,
		      centerPadding: 'calc(50vw - 480px)',
		      slidesToShow: 1,
		      dots: true,
		      responsive: [
		        {
		          breakpoint: 960,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '20px',
		            slidesToShow: 1
		          }
		        },
		        {
		          breakpoint: 767,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '15px',
		            slidesToShow: 1,
		            dots: false,
		          }
		        }
		      ]
		    });
		  }
		  if($('#owl-demo').length && scrollbar == 17){
		    $('#owl-demo').slick({
		      centerMode: true,
		      centerPadding: 'calc(50vw - 488.5px)',
		      slidesToShow: 1,
		      dots: true,
		      responsive: [
		        {
		          breakpoint: 960,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '20px',
		            slidesToShow: 1
		          }
		        },
		        {
		          breakpoint: 767,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '15px',
		            slidesToShow: 1,
		            dots: false,
		          }
		        }
		      ]
		    });
		  }
	});
}(jQuery));