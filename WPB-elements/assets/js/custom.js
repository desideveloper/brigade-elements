
(function( $ ) {
    // $('.tabular-container')
    $('.tabular-tabs ul li').each(function(){
        var parentIndex=$(this).parents('.tabs-wrapper').index()+"a";
        var number =$(this).index();
        $(this).attr('data-id', "tab"+parentIndex + number).addClass('btn');
    });
    $('.tabular-contents > div').each(function(){
        $(this).addClass('individual-content');
        var number =$(this).index();
        var navTxt=$('.tabular-tabs ul').children('li[data-id="tab'+number+'"]').html();
        var parentIndex=$(this).parents('.tabs-wrapper').index()+"a";
        var number =$(this).index();
        $(this).attr('id', "tab"+parentIndex + number);
        $(this).prepend('<h2>'+navTxt+'</h2>');
        $(this).children('.js-select').attr('data-clipboard-target', "#js-selcetion-"+$(this).attr('id'));
        $(this).children('.js-selection').attr('id', "js-selcetion-"+$(this).attr('id'))
    });
    $('.tabular-tabs ul li:first-child').addClass('active-tab');
    $('.tabular-contents > div.individual-content:first-child').show();
    $('.tabular-tabs ul li').on('click', function(){
        var id=$(this).data('id');
        if(!$(this).hasClass('active-tab')){
            $(this).siblings().removeClass('active-tab');
            $(this).addClass('active-tab');
            $('#'+id).siblings('.individual-content').hide();
            $('#'+id).fadeIn();
        }
    });


    var clipboard = new Clipboard('.js-select');

    clipboard.on('success', function(e) {
        console.log(e);
        // $('[data-toggle="tooltip"]').tooltip('show');
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });

    $('.show-hide-selection').on('click', function(){
      $('.hover-wrapper').contents().unwrap();
        $(this).text(function(i, text){
            return text === "View Source" ? "Hide Source ↘" : "View Source";
        });
        $(this).parent().next('.tabs-wrapper').toggle().toggleClass('show-side');
        $(this).parents('.container').toggleClass('show-side');
        $(this).parents('.container').prev('style').toggleClass('show-side');
        $(this).parents('.container').prev().prev('.html').toggleClass('show-side');
        $('body').toggleClass('panel-active');
        if($(this).parents('.hover-inner').length){
          $('.hover-wrapper').contents().unwrap();
          $('.hover-inner').contents().unwrap();

        }
        else{
          $('.show-side').wrapAll('<div class="hover-wrapper"><div class="hover-inner"></div></div>');
        }
    });


 


     
 


    $(window).on('load', function(){
      $('.tabs-wrapper').each(function(){
        var html =$(this).find('.html-contents').children('.selectionbox').val(),
            css =$(this).find('.css-contents').children('.selectionbox').val(),
            js =$(this).find('.css-contents').children('.selectionbox').val();

        /**
         *
         * html block
         *
         */
        $('<div class="html">'+html+'</div>').insertBefore($(this).prev('.container'));
        /**
         *
         * css block
         *
         */
         $('<style>'+css+'</style>').insertBefore($(this).prev('.container'));
      });
    });

    $('.tabs-wrapper .html-contents .selectionbox').on('keyup', function(){
      $(this).parents('.tabs-wrapper').prev().prev().prev('.html').html($(this).val());
    });
    $('.tabs-wrapper .css-contents .selectionbox').on('keyup', function(){
      $(this).parents('.tabs-wrapper').prev().prev('style').html($(this).val());
    });


})(jQuery);
