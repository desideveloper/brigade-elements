(function($){
	$(function(){
		function getScrollbarWidth() {
		    var outer = document.createElement("div");
		    outer.style.visibility = "hidden";
		    outer.style.width = "100px";
		    document.body.appendChild(outer);

		    var widthNoScroll = outer.offsetWidth;
		    // force scrollbars
		    outer.style.overflow = "scroll";

		    // add innerdiv
		    var inner = document.createElement("div");
		    inner.style.width = "100%";
		    outer.appendChild(inner);

		    var widthWithScroll = inner.offsetWidth;

		    // remove divs
		    outer.parentNode.removeChild(outer);

		    return widthNoScroll - widthWithScroll;
		  }

		  // document.body.innerHTML = "Scrollbar width is: "+getScrollbarWidth()+"px";
		  var scrollbar = getScrollbarWidth();
		  if($('#owl-demo').length && scrollbar == 0){
		    $('#owl-demo').slick({
		      centerMode: true,
		      centerPadding: 'calc(50vw - 480px)',
		      slidesToShow: 1,
		      dots: true,
		      responsive: [
		        {
		          breakpoint: 960,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '20px',
		            slidesToShow: 1
		          }
		        },
		        {
		          breakpoint: 767,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '15px',
		            slidesToShow: 1,
		            dots: false,
		          }
		        }
		      ]
		    });
		  }
		  if($('#owl-demo').length && scrollbar == 17){
		    $('#owl-demo').slick({
		      centerMode: true,
		      centerPadding: 'calc(50vw - 488.5px)',
		      slidesToShow: 1,
		      dots: true,
		      responsive: [
		        {
		          breakpoint: 960,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '20px',
		            slidesToShow: 1
		          }
		        },
		        {
		          breakpoint: 767,
		          settings: {
		            arrows: true,
		            centerMode: true,
		            centerPadding: '15px',
		            slidesToShow: 1,
		            dots: false,
		          }
		        }
		      ]
		    });
		  }
	});
	$('.wpb_slidewrapper_3').slick({
		  centerMode: true,
		  arrows: true,
		  centerPadding: '0px',
		  slidesToShow: 3,
		  dots: true,
		  responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 1
		      }
		    }
		  ]
		});
	$(".wpb_numberSlider_4").owlCarousel({
    navigation : false,
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:true
  });
	$('.wpb_flexslider_2').flexslider({
	      animation: "slide",
	      controlNav: false,
	      start: function(slider){
	        $('body').removeClass('loading');
	      }
	    });
	    var galleryItems = $('.cd-gallery');

	      galleryItems.each(function(){
	        var container = $(this),
	          // create slider dots
	          sliderDots = createSliderDots(container);
	        //check if item is on sale
	         
	        container.find('.cd-item-wrapper li:nth-child(1)').addClass("move-left").next().addClass("selected").next().addClass("move-right");


	        // update slider when user clicks one of the dots
	        sliderDots.on('click', function(){
	          var selectedDot = $(this);
	          if(!selectedDot.hasClass('selected')) {
	            var selectedPosition = selectedDot.index(),
	              activePosition = container.find('.cd-item-wrapper .selected').index();
	            if( activePosition < selectedPosition) {
	              nextSlide(container, sliderDots, selectedPosition);
	            } else {
	              prevSlide(container, sliderDots, selectedPosition);
	            }

	             
	          }
	        });

	        // update slider on swipeleft
	        container.find('.cd-item-wrapper').on('swipeleft', function(){
	          var wrapper = $(this);
	          if( !wrapper.find('.selected').is(':last-child') ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() + 1;
	            nextSlide(container, sliderDots);
	             
	          }
	            if(container.find('.move-right').nextAll('li').length<=1){
	              container.find('.hide-left:first-child').removeClass('hide-left').appendTo(container.find('.cd-item-wrapper'));
	            }
	        });
	        // update slider on swiperight
	        container.find('.cd-item-wrapper').on('swiperight', function(){
	          var wrapper = $(this);
	          if( !wrapper.find('.selected').is(':first-child') ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() - 1;
	            prevSlide(container, sliderDots);
	             
	          }

	            if(container.find('.hide-left').length<=1){
	              container.find('.cd-item-wrapper li:last-child').addClass('hide-left').prependTo(container.find('.cd-item-wrapper'));
	            }
	        });

	        // preview image hover effect - desktop only
	        container.on('mouseover', '.move-right, .move-left', function(event){
	          hoverItem($(this), true);
	        });
	        container.on('mouseleave', '.move-right, .move-left', function(event){
	          hoverItem($(this), false);
	        });

	        // update slider when user clicks on the preview images
	        container.on('click', '.move-right, .move-left, .wpb-arrow-left, .wpb-arrow-right', function(event){
	          event.preventDefault();

	          var index_positioon = container.find('.cd-item-wrapper .selected').index(),
	            slides_count    = container.find('.cd-item-wrapper li').length

	            console.log(index_positioon);
	            console.log(slides_count);

	          if ( $(this).hasClass('move-right') ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() + 1;
	            nextSlide(container, sliderDots);
	            if(container.find('.move-right').nextAll('li').length<=1){
	              container.find('.hide-left:first-child').removeClass('hide-left').appendTo(container.find('.cd-item-wrapper'));
	            }
	          }
	          if ( $(this).hasClass('move-left') ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() - 1;
	            prevSlide(container, sliderDots);
	            if(container.find('.hide-left').length<=1){
	              container.find('.cd-item-wrapper li:last-child').addClass('hide-left').prependTo(container.find('.cd-item-wrapper'));
	            }
	          }
	          if ( $(this).hasClass('wpb-arrow-right') && slides_count - 1 > index_positioon  ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() + 1;
	            nextSlide(container, sliderDots);
	            if(container.find('.move-right').nextAll('li').length<=1){
	              container.find('.hide-left:first-child').removeClass('hide-left').appendTo(container.find('.cd-item-wrapper'));
	            }
	          }
	          if ( $(this).hasClass('wpb-arrow-left') && index_positioon > 0 ) {
	            var selectedPosition = container.find('.cd-item-wrapper .selected').index() - 1;
	            prevSlide(container, sliderDots);
	            if(container.find('.hide-left').length<=1){
	              container.find('.cd-item-wrapper li:last-child').addClass('hide-left').prependTo(container.find('.cd-item-wrapper'));
	            }
	          }
	           
	        });
	      });

	      function createSliderDots(container){
	        var dotsWrapper = $('<ol class="cd-dots"></ol>').insertAfter(container.children('ul'));
	        container.find('.cd-item-wrapper li').each(function(index){
	          var dotWrapper = (index == 1) ? $('<li class="selected"></li>') : $('<li></li>'),
	            dot = $('<a href="#0"></a>').appendTo(dotWrapper);
	          dotWrapper.appendTo(dotsWrapper);
	          dot.text(index+1);
	        });
	        setTimeout(function(){
	          if(container.find('.hide-left').length<1 && !$('.cd-item-wrapper li').length<4){
	            container.find('.cd-item-wrapper li:last-child').addClass('hide-left').prependTo(container.find('.cd-item-wrapper'));
	          }
	        },3000)
	        if(container.find('.cd-item-wrapper li').length<4){
	          container.find('.wpb-arrow-left').hide();
	          container.find('.wpb-arrow-right').hide();
	        }
	        return dotsWrapper.children('li');
	      }

	      function hoverItem(item, bool) {
	        ( item.hasClass('move-right') )
	          ? item.toggleClass('hover', bool).siblings('.selected, .move-left').toggleClass('focus-on-right', bool)
	          : item.toggleClass('hover', bool).siblings('.selected, .move-right').toggleClass('focus-on-left', bool);
	      }

	      function nextSlide(container, dots, n){
	        if(container.find('.move-right').nextAll('li').length>0){
	          var visibleSlide = container.find('.cd-item-wrapper .selected'),
	            navigationDot = container.find('.cd-dots .selected');
	          if(typeof n === 'undefined') n = visibleSlide.index() + 1;
	          visibleSlide.removeClass('selected');
	          container.find('.cd-item-wrapper li').eq(n).addClass('selected').removeClass('move-right hover').prevAll().removeClass('move-right move-left focus-on-right').addClass('hide-left').end().prev().removeClass('hide-left').addClass('move-left').end().next().addClass('move-right');
	          navigationDot.removeClass('selected')
	          dots.eq(n).addClass('selected');
	        }
	      }

	      function prevSlide(container, dots, n){
	        if(container.find('.move-left').prevAll('li').length>0){
	          var visibleSlide = container.find('.cd-item-wrapper .selected'),
	            navigationDot = container.find('.cd-dots .selected');
	          if(typeof n === 'undefined') n = visibleSlide.index() - 1;
	          visibleSlide.removeClass('selected focus-on-left');
	          container.find('.cd-item-wrapper li').eq(n).addClass('selected').removeClass('move-left hide-left hover').nextAll().removeClass('hide-left move-right move-left focus-on-left').end().next().addClass('move-right').end().prev().removeClass('hide-left').addClass('move-left');
	          navigationDot.removeClass('selected');
	          dots.eq(n).addClass('selected');
	        }
	      }
	      $('.wpb_banner-slider-6').slick({
		      arrows: false,
		      autoplay: true,
	        autoplaySpeed: 2000,
	      });
	      $('.wpb_banner-slider-6').on('init', function(e, slick) {
            var $firstAnimatingElements = $('div.wpb_main-slider_6[data-slick-index="0"').find('[data-animation]');
            doAnimations($firstAnimatingElements);    
        });
        $('.wpb_banner-slider-6').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
          var $animatingElements = $('div.wpb_main-slider_6[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
          doAnimations($animatingElements);    
        });
        function doAnimations(elements) {
          var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
          elements.each(function() {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function() {
                $this.removeClass($animationType);
            });
          });
        }
        $('.wpb_slider-for_7').slick({
	        draggable: true,
	        accessibility: false,
	        centerMode: true,
	        variableWidth: true,
	        slidesToShow: 1,
	        arrows: true,
	        dots: false,
	        swipeToSlide: true,
	        asNavFor: '.wpb_slider-nav_7',
	        appendArrows: $('.wpb_thumbnail-slider-wrapper_7'),
	        responsive: [
	            {
	                breakpoint: 1063,
	        				settings: {
	        					variableWidth: false,
	        					centerPadding: '0',
	        					adaptiveHeight: true
	        				}
	            }
	        ]

	      });
	      $('.wpb_slider-nav_7').slick({
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        asNavFor: '.wpb_slider-for_7',
	        dots: false,
	        centerMode: true,
	        variableWidth: true,
	        focusOnSelect: true,
	        arrows: false,
	      });


	      /*=========================================
	      =            Code for slider 8            =
	      =========================================*/
	      
	      $('.wpb_slickslide_8').slick({
	              dots: true,
	              infinite: true,
	              speed: 500,
	              fade: false,
	              slide: 'li',
	              cssEase: 'linear',
	              centerMode: true,
	              slidesToShow: 1,
	              variableWidth: true,
	              autoplay: true,
	              autoplaySpeed: 4000,
	              arrows: false,
	              responsive: [{
	                  breakpoint: 800,
	                  settings: {
	                      arrows: false,
	                      centerMode: false,
	                      centerPadding: '40px',
	                      variableWidth: false,
	                      slidesToShow: 1,
	                      dots: true,
	              arrows: false,
	                  },
	                  breakpoint: 1200,
	                  settings: {
	                      arrows: false,
	                      centerMode: false,
	                      centerPadding: '40px',
	                      variableWidth: false,
	                      slidesToShow: 1,
	                      dots: true,
	              arrows: false

	                  }
	              }],
	              customPaging: function (slider, i) {
	                  return '<button class="wpb_tab_8">' + $('.wpb_slick-thumbs_8 li:nth-child(' + (i + 1) + ')').html() + '</button>';
	              }
	          });
	      
	      /*=====  End of Code for slider 8  ======*/
	      
}(jQuery));