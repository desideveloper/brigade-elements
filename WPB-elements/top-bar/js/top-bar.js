var timer;

var compareDate = new Date();
compareDate.setDate(compareDate.getDate() + 7); //just for this demo today + 7 days

timer = setInterval(function() {
  timeBetweenDates(compareDate);
}, 1000);

function timeBetweenDates(toDate) {
  var dateEntered = toDate;
  var now = new Date();
  var difference = dateEntered.getTime() - now.getTime();

  if (difference <= 0) {

    // Timer done
    clearInterval(timer);
  
  } else {
    
    var seconds = Math.floor(difference / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours %= 24;
    minutes %= 60;
    seconds %= 60;

    $(".wpb_countdown_days").text(days);
    $(".wpb_countdown_hours").text(hours);
    $(".wpb_countdown_minutes").text(minutes);
    $(".wpb_countdown_seconds").text(seconds);
  }
}
$('.wpb_close_notice_top_bar').on('click', function(){
  $(this).closest('.wpb_notice_top_bar').hide();
if($('.wpb_notice_top_bar').length>0){
  $('html').css('padding-top', '0px'); 
}
});
if($('.wpb_notice_top_bar').length>0){
  $('html').css('padding-top', $('.wpb_notice_top_bar').height() + 'px'); 
}
