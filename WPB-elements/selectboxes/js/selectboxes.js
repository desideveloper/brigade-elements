(function($){
    $(function(){
        $('.selectbox3, .selectbox2').each(function () {
            // Cache the number of options
            var $this = $(this),
            numberOfOptions = $(this).children('option').length;

            // Hides the select element
            $this.addClass('s-hidden');

            // Wrap the select element in a div
            $this.wrap('<div class="select"></div>');

            // Insert a styled div to sit over the top of the hidden select element
            $this.after('<div class="styledSelect"></div>');

            // Cache the styled div
            var $styledSelect = $this.next('div.styledSelect');
            var getHTML = $this.children('option[value="'+$this.val()+'"]').text();
            // Show the first select option in the styled div
            if($this.children('option[selected="selected"]').length>0){
                $styledSelect.text(getHTML);
            }else if ($this.attr('data-placeholder')){
                $styledSelect.text($this.data('placeholder'));
            }
            else{
                $styledSelect.text(getHTML);
            }

            // Insert an unordered list after the styled div and also cache the list
            var $list = $('<ul />', {
              'class': 'options'
            }).insertAfter($styledSelect);

            // Insert a list item into the unordered list for each select option
            for (var i = 0; i < numberOfOptions; i++) {
              $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
              }).appendTo($list);
            }

            // Cache the list items
            var $listItems = $list.children('li');

            // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
            $styledSelect.click(function (e) {
              e.stopPropagation();
              $('div.styledSelect.active').each(function () {
                  $('ul.options').slideUp();
              });
                if(!$(this).hasClass('active')){
                    $(this).addClass('active').next('ul.options').slideDown();
                }else{
                    $(this).removeClass('active').next('ul.options').slideUp();
                }
            });

            // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
            // Updates the select element to have the value of the equivalent option
            $listItems.click(function (e) {
              e.stopPropagation();
              $styledSelect.text( $(this).text()).removeClass('active');
              var value = $(this).attr('rel').toString();
              $($this).val(value);
              $($this).trigger('change');
              $list.slideUp();
              $(this).addClass('active').siblings().removeClass('active');
            });

            // Hides the unordered list when clicking outside of it
            $(document).click(function () {
              $styledSelect.removeClass('active');
              $list.slideUp();
            });

          });

    });
}(jQuery));