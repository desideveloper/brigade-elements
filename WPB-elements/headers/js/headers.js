(function($){
	$(document).ready(function(){
		$('.wpb_navTrigger-1').on('click', function  () {
	    $(this).toggleClass('active');
	    $('.wpb_header-nav-1').toggleClass('wpb_slide-in-1');
	    // $('.mainMenu').toggleClass('open');
	  });
	  $('.wpb_header-nav-1 li').each(function  () {
	    if($(this).children().length !== 1){
	      $(this).addClass('dropdownmenu-1');
	    }
	  });
	  $(document).on('click', '.dropdownmenu-1>a', function  (e) {
	    console.log($(window).width() < 768);
	    if($(window).width() < 768){
	      e.preventDefault();
	      $(this).next().slideToggle();
	    }
	  });
	  $(document).click(function (e) {
	  	$('.wpb_search_form-1').removeClass('expanded');
	  		e.stopPropagation();
	    });
	    $(".wpb_search_btn-1").click(function (e) {
	      $(".wpb_search_form-1").addClass("expanded");
	      e.stopPropagation();
	    });
	    $(".wpb_search_form-1").click(function (e) {    
	      e.stopPropagation();
	    });
	    $('.wpb_menuButton-2 ').on('click', function  () {
    		$(this).toggleClass('active');
    		$('.wpb_mega_menu-2').slideToggle();
	    });
	    $('.wpb_main_menu_items-2 li a').on('click', function  (e) {
    		e.preventDefault();
    		$(this).next().slideToggle();
    		$(this).parent().siblings().children('ul').slideUp();
    	});
    	$('.wpb_login_nav-2 li.wpb_search-2').on('click', function  () {
    		$('.wpb_searchForm-2').toggle();
    	});
    	$('.wpb_closeicon-2').on('click', function  () {
    		$('.wpb_searchForm-2').hide();
    	});
    	$('#wpb_nav-icon3').on('click', function (e) {
    	    // $(this).toggleClass('open');
    	    if(!$(this).hasClass('open')){
    	      $(this).addClass('open');
    	      $('.wpb_top-menu-bar-3').addClass('wpb_full-menu-3');
    	      e.stopPropagation();
    	    }
    	    else{
    	      $(this).removeClass('open');
    	      $('.wpb_top-menu-bar-3').removeClass('wpb_full-menu-3');
    	      e.stopPropagation();
    	    }
    	  });
    	  $(".wpb_top-menu-bar-3").click(function(e){
    	    e.stopPropagation();
    	  });
    	  $(document).click(function(){
    	    $('#wpb_nav-icon3').removeClass('open');
    	      $(".wpb_top-menu-bar-3").removeClass('full-menu');
    	  });
    	  $('.wpb_has-menu-3 a').on('click', function (e) {
    	    if($(window).width()<=767){
    	      e.preventDefault();
    	      $(this).toggleClass('wpb_menu-open-3').next().slideToggle();
    	    }
    	  });
    	  $('.wpb_has-menu-3').on('mouseover', function (e) {
    	    $(this).closest('.top-menu-bar').addClass('overlay-active');
    	  }).on('mouseleave', function () {
    	    $(this).closest('.top-menu-bar').removeClass('overlay-active');
    	  });
    	  $('.right-menus ul li a').on('click', function(){
    	    $(this).parent().addClass('active').siblings().removeClass('active');
    	  });

    	  $('.wpb_menu-btn-4').on('click', function () {
    	  	$('.wpb_main-menu-4').slideToggle();
  	  	});
  	  	$('.wpb_main-menu-4 > li').each(function () {
  	  		if($(this).children('.wpb_dropdown-4').length>0){
  	  			$(this).addClass('wpb_has-child-4');
  	  		}
  	  	});
  	  	$(document).on('click', '.wpb_has-child-4>a',function (e) {
					if($(window).width()<=767){
						e.preventDefault();
						$(this).next('.wpb_dropdown-4').slideToggle();
					}
				});
        $('.wpb_menu-btn-5').on('click', function () {
            $('.wpb_right-menu-section-5').slideToggle();
        });
        $(document).on('click','.wpb_right-tab-5>span', function () {
            $(this).next().toggle();
        });
        $('.wpb_searchForm-5').on('submit', function (e) {
            if(!$(this).hasClass('in')){
                e.preventDefault();
                $(this).addClass('in');
                $(this).find('input[type="search"]').focus();
            }
            else{
                $(this).removeClass('in');
            }
        });
        $(document).on('click', function () {
            $('.wpb_searchForm-5').removeClass('in');
        });
        $('.wpb_searchForm-5').on('click', function (e) {
            e.stopPropagation();
        });
        $(document).on('click','.wpb_right-tab-5 ul li>a', function () {
            var getHtml = $(this).html();
            $(this).closest('ul').hide();
            $('.wpb_right-tab-5>span').html(getHtml);
        });
	});
}(jQuery));