(function($){

        $.fn.chunkify = function( options ) {

                $.fn.chunkify.settings = {
                    separator: /\s/,
                    wrapper: 'span',
                    klass: 'chunk'
                };

                options = $.extend( $.fn.chunkify.settings, options );

                return this.each(function() {

                    var $element = $( this );
                    var num = 0;
                    var text = $element.text();


                    var parts = text.split( options.separator );
                    var html = "";

                    for( var i = 0; i < parts.length; ++i ) {

                        num++;

                        var part = parts[i];

                        html += "<" + options.wrapper + " class='" + options.klass + num + "'>" + part + "</" + options.wrapper + "> ";


                    }

                    $element.html( html );

                });


        };


            /**
            * span-letters.js
            *
            * Example usage: jQuery('.selector').spanLetters();
            */
            $.fn.spanLetters = function() {

                // Loop through each element on which this function has been called
                this.each( function() {

                    // Scope the variables
                    var words, i, text;

                    // Make an array with each letter of the string as a value
                    words = $( this ).text().split( '' );

                    // Loop through the letters and wrap each one in a span
                    for ( i = 0; i in words; i++ ) {
                        words[i] = '<span data-list="anime'+ ( i + 1 ) +'" class="sl' + ( i + 1 ) + ' span-letter">' + words[i] + '</span>'
                    };

                    // Join our array of span-wrapped letters back into a string
                    text = words.join( '' );

                    // Replace the original string with the new string
                    $( this ).html( text );
                });
            };
            $.fn.isVisible = function() {
                // Am I visible?
                // Height and Width are not explicitly necessary in visibility detection, the bottom, right, top and left are the
                // essential checks. If an image is 0x0, it is technically not visible, so it should not be marked as such.
                // That is why either width or height have to be > 0.
                var rect = this[0].getBoundingClientRect();
                return (
                    (rect.height > 0 || rect.width > 0) &&
                    rect.bottom >= 0 &&
                    rect.right >= 0 &&
                    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
                );
            };

        $(function() {
        $( ".anim h2" ).chunkify();
        $( ".anim h2 span" ).spanLetters();
        var ul = $('.anim>h2');
		    ul.each(function(){
                $(this).children('span').children('span').addClass(function(index){
                    var group_number = index;
                    return 'tranistion-delay-'+ group_number;
                });
            });
        $('.anim-img').each(function(){
            var anim_img = $(this);
            var anim_section = anim_img.find('.anim-thumbnail').height()/anim_img.width()*100;
            anim_img.append('<div class="anim-placeholder"></div>');
            anim_img.find('.anim-thumbnail').wrap('<div class="anim-thumbnail-wrapper"></div>');
            anim_img.find('.anim-placeholder').css('padding-bottom',anim_section + '%' );
        });
        $(window).on('load scroll',function(){
            var headings = $('.anim h2');
            headings.each(function(i, el) {
            var $this = $(this);
                if ($this.isVisible()) {
                    $(this).addClass('anim-hd');
                }
            });
            var anim_img = $('.anim-img');
            anim_img.each(function(i, el) {
            var $this = $(this);
                if ($this.isVisible()) {
                    $(this).find('.anim-thumbnail').attr('class', 'after-anim-thumbnail anim-thumbnail');
                }
            });
        });

        });
}(jQuery));