$(document).ready(function(){
    if($('.wpb_slideWrapper').length>0){
        $('.wpb_slideWrapper').owlCarousel({
            loop:true,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    dot:true,
                },
                600:{
                    items:2,
                    dot:true,
                },
                1000:{
                    items:3,
                    dot:true,
                    loop:false
                }
            }
        });
    }
});